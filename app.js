// 1.

const text = document.body.querySelectorAll('p');

for (let i of text) {
    i.style.backgroundColor = "#ff0000";
}

// 2.

let optionsListEls = document.getElementById("optionsList");
console.log(optionsListEls);
console.log(optionsListEls.parentElement);
if (optionsListEls.hasChildNodes()) {
	let optionsListChildNodes = optionsListEls.childNodes;
	let nodeNames = "";
	for (let i of optionsListChildNodes) {
		nodeNames += i.nodeName + " ";
	}
	console.log(`Node names: ${nodeNames}`);
	let nodeTypes = "";
	for (let i of optionsListChildNodes) {
		nodeTypes += i.nodeType + " ";
	}
	console.log(`Node types: ${nodeTypes}`);
}
document.querySelector(".testParagraph").innerText = "This is a paragraph";
let mainHeaderEl = document.querySelector(".main-header").children;
console.log(mainHeaderEl);
for (let i of mainHeaderEl) {
	i.classList.add("nav-item");
}
console.log(mainHeaderEl);
let sectionTitleEl = document.querySelectorAll(".section-title");
console.log(sectionTitleEl);
for (let i of sectionTitleEl) {
	i.classList.remove("section-title");
}
console.log(sectionTitleEl);
